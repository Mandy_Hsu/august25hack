package LibraryProject;

public class Periodicals extends LibraryO {
    
    public Periodicals(String name) {
        super(name, "Periodicals");
    }

    @Override
    public boolean checkout(String n){
        System.out.println("Periodicals may not be checked out from the liibrary");
      return false;  
    } 

    @Override
    public boolean checkIn(String name){
        System.out.println("Periodicals may not be checked out from the liibrary");
      return false;
    }

    @Override
    public String toString(){
        return this.name+" is a periodical and may not be checked out";
    }
}