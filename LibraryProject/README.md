This is a readme for the Library Project.

The design is rather simple. I am assuming the following things: 

1.) There are no requirements for an item to be checked out 
    a.) With an exception to Periodicals
2.) An item only requires a name to be checked out. It does not have any additional restraints


To demonstrate what it would look like, I created a LibraryO array in the main method, which exists in 
the Library class. The main reason to create a LibraryO abstract class is so we can easily create an array 
, regardless of what types of items they are. Originally, I considered creating an interface instead so Periodicals 
can extend a separate class. However, given that it is the sole exception, I decided to just use the same
abstract class and allowed all items to inherit from LibraryO. 