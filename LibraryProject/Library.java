package LibraryProject;

public class Library {

    public static void main(String[] args) {
        LibraryO[] library = new LibraryO[4];

        library[0] = new Books("Tempest");
        library[1] = new CDs("Mother Goose");
        library[2] = new DVDs("Avatar the Last Airbender");
        library[3] = new Periodicals("Time Magazine");

        System.out.println(toString(library)+"\n\n\n\n");

        int randomPick = (int)(Math.random()*3);

        testCheckout(library[randomPick], "Mandy");

        remove(library, "Time Magazine");

        System.out.println("\n\n\n\n"+toString(library));
    }

    public static String toString(LibraryO[] l){
        String toReturn;

        if (l.length == 0){
            toReturn = "This library is currently empty.";
        }else{
            toReturn = "The full library: \n";

            
            for (int i =0; i<l.length; i++){
                if (l[i] != null){
                toReturn += l[i].name+", "+l[i].type+"\n";
                }
            }
        }

        return toReturn;
    }

    /**
     * Simulating check in/out
     * @param obj
     * @param n
     */
    public static void testCheckout(LibraryO obj, String n){
        System.out.println("Checking out "+obj.name+"\n"+obj.checkout(n) +"\n"+obj.toString());

        System.out.println(("Checking it back in...\n"+obj.checkIn(n))+"\n"+obj.toString());

    }

    /**
     * Removing if you know what the index is
     * @param i
     */
    public static void remove(LibraryO[] lib, int i){
        if (i < lib.length){
            LibraryO removed = lib[i];

            if(i == lib.length-1){
                lib[i] = null;
            }else{
                lib[i] = lib[lib.length-1];
                lib[lib.length-1] = null;
            }
            System.out.println(removed.name+" has been removed from the library\n");
        }else{
            System.out.println("No items at that index\n");
        }
    }

    /**
     * removing by name of the item
     * @param name
     */
    public static void remove(LibraryO[] lib, String n){
        int index = -1;
        for (int i = 0; i < lib.length; i++){
            if(lib[i].name.equals(n)){
                index = i;
                break;
            }
        }
        if(index ==-1){
            System.out.println(n+" is not in the library\n");
        }else{
            remove(lib, index);
        }
    }

        
}
