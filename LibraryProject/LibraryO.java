package LibraryProject;

public abstract class LibraryO {
    String name;
    String type;

    boolean checkedOut;

    String current_holder;

    public LibraryO(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public boolean checkout(String n){
        this.checkedOut = true;
        this.current_holder = n;

        return true;
        
    } 

    public boolean checkIn(String name){
        this.checkedOut = false;
        this.current_holder = null;
        return true;
    }

    public String toString(){
        if(this.checkedOut){
            return this.name+" has been checked out. Please come back another time.";
        }else{
            return this.name+" is available. You may check it out";
        }
    }
}